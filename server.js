//Inicialización
const express = require('express');
const server = express()

const path = require('path')
const helmet = require('helmet')
const cors = require('cors')
const passport = require('passport');
server.use(passport.initialize());

const swaggerUI = require('swagger-ui-express');
const swaggerJson = require('./utils/swagger.json')

const {errorHandler, logErrors} = require('./src/middlewares/errorHandler')

const port = require('./config/index').config.port

const apiRoutes = require('./src/routes/index')
const mercadoPago = require('./src/routes/mercadopago')


//Middlewares
server.use(cors())
server.use(express.json())
server.use(express.urlencoded({extended:true}))
server.use(
        helmet({
                contentSecurityPolicy: false,
        })
)
server.use(passport.initialize())

//running front
server.use(express.static(path.join(__dirname, 'public')))



//routes
server.use('/', mercadoPago)
server.use('/api', apiRoutes)
server.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerJson)); //sino funciona hay que agregar SwaggerOptions otra vez

//error handler
server.use(logErrors)
server.use(errorHandler)




server.listen(port, (req,res) => {
console.log(`Server running at ${process.env.DOMAIN || 'http://localhost'}:${port}`)
})

