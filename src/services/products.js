const productsModel = require('../library/model/products')

class ProductsService {
    
    async getProducts() {
    
        const products = await productsModel.find({}).lean()
        return products || []

    }

    async newProduct(food_name, food_price){
    
        let newProduct = {
            food_name,
            food_price
        }


        try {
            const productCreation = await productsModel.create(newProduct)
            return {
                status: 200,
                data:productCreation,
                message: 'product added'
            }
        } catch (error)     {
            return {
                status: 400,
                data: error,
                message: 'product couldn`t be added'
            }
        }
    }

    async modifiedProduct(id, food_name, food_price){
    
        let productModified = {
            food_name,
            food_price
        }

        try {
            const modifiedProduct = await productsModel.findByIdAndUpdate(id, productModified, {new: true})
            return {
                status: 200,
                data:modifiedProduct,
                message: 'product Modified'
            }
        } catch (error)     {
            return {
                status: 400,
                data: error,
                message: 'product couldn`t be modified'
            }
        }
        
    }

    async deleteProduct(id){
        
        try {
            const productDeleted = await productsModel.findByIdAndRemove(id)
            return {
                status: 200,
                data: productDeleted,
                message: 'product Deleted'
            }
        } catch (error) {
            return {
                status: 400,
                data: error,
                message: 'product couldn`t be deleted'
            }
        }

    }
}

module.exports = ProductsService;