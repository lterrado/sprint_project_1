const path = require('path')
const passport = require('passport')
const passportGoogle = require('passport-google-oauth20');
const googleIds = require(path.join(__dirname,'../../config/index')).config
const GoogleStrategy = passportGoogle.Strategy



passport.use('google',new GoogleStrategy({
        clientID: googleIds.googleClientID,
        clientSecret: googleIds.googleSecretID,
        callbackURL: googleIds.googleCallback,
        passReqToCallback: true

  },
    function(request,accessToken, refreshToken, profile, done) {
        return done(null, profile)
    }
))

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

