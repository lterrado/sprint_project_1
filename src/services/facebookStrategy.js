const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy;
const facebookID = require('../../config/index').config


passport.use(new FacebookStrategy({
    clientID: facebookID.facebookClientID,
    clientSecret: facebookID.facebookClientSecret,
    callbackURL: facebookID.facebookCallback,
    passReqToCallback: true
},
function(request, accessToken, refreshToken, profile, done) {
    return done(null, profile);
}));

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});