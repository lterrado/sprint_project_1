const ordersModel = require('../library/model/orders')
const productsModel = require('../library/model/products')

class OrdersServices{
    
    async getOrders(user){

        const orderListComplete = await ordersModel.find({}).lean()
        
        if (user.admin) {

            return orderListComplete
        } 
        else {
            
            const  userOrders = (orders) => orders.userId == user.id
            let ordersByUsers = orderListComplete.filter(userOrders)

            return ordersByUsers
        }
    }

    async verifiedProductExist(productsArray){

        try {
            const ids = productsArray.map(item => item.product_id)
            let result = await productsModel.find({ _id: { $in: ids }}).lean()
            return result
        } catch (error) {
            return false
        }
    }

    async createOrder(userId, order_details, order_State, paymentsId, addressDelivery){

        let productsOk = await this.verifiedProductExist(order_details)

        if (productsOk) {
            let newOrder = {
                userId,
                order_details,
                order_State,
                paymentsId,
                addressDelivery,
            }
            
            try {
                const orderCreatedNew = await ordersModel.create(newOrder)
    
                return {
                    status: 200,
                    data: orderCreatedNew,
                    message: 'New Order Created'
                }
            } catch (error) {
                return {
                    status: 400,
                    data: error,
                    message: 'New Order couldn`t be created'
                }
            }
        } else {
            return {
                status: 400,
                data: {},
                message: 'New Order couldn`t be created, wrong product ID'
            }
        }

    }

    async changeOrderState(orderId, orderState){

        try {
            
            const orderStatusChange = await ordersModel.findByIdAndUpdate(orderId, {order_State: orderState}, {new: true})
            
            return {
                status: 200,
                data: orderStatusChange,
                message: 'Status Order Change'
            }

        } catch (error) {
            return {
                status: 400,
                data: error,
                message: 'Status couldn`t be change'
            }
            
        }
    }



}

module.exports = OrdersServices
 