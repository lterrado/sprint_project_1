const UserModel = require('../library/model/users')
const { config } = require('../../config/index')

const jwt = require('jsonwebtoken')

const encrypt = require('../../utils/encrypt')

class usersService {

    async getUsers() {

        const users = await UserModel.find({}).lean()
        return users || []

    }

    async newUsers(user,name,lastName,email,phoneNumber,address,plainPass) {
        
        //Verify Duplicate Email
        const isEmailExist = await UserModel.findOne({ email });
        
        if (isEmailExist) { return {
            status: 400,
            data: null,
            message: 'Duplicated Email '
        }}

        //Encrypting password

        const pass = await encrypt.encrypt(plainPass)

        // create user Object
        let newUser = {
            user,
            name,
            lastName,
            email,
            phoneNumber,
            address,
            pass,
            admin: false,
        }
        
        try {
            const user = await UserModel.create(newUser)
            return {
                status: 200,
                data: user,
                message: 'User registered'
            }

        } catch(err) {
            console.error(err)
            return {
                status: 400,
                data: err,
                message: 'Not possible to register'
            }
        }
        
    }


    async login(user,pass){
        
        const userFound = await UserModel.findOne( { user } ).lean()

        if (userFound && await encrypt.verifiedEncryptedPass(pass, userFound.pass)) {
            
            const userToken = jwt.sign({
                id: userFound._id,
                username: userFound.user
            }, 
                config.jwtToken
                //{expiresIn: 600}
            )

            return {
                status: 200,
                data: userToken,
                message: 'User Log In'
            }

        } else {
            return {
                status: 400,
                data: null,
                message: ' Login Data incorrect '
            }
        }
        
    }

    async setNewAddress(userId, newAddress) {
        try {
            const userFound = await UserModel.findById( userId )

            const userNewAddressArray = userFound.address
    
            userNewAddressArray.push(newAddress)
    
            const userWithNewAddress = await UserModel.findByIdAndUpdate(userId, {address: userNewAddressArray}, {new: true})

            return {
                status: 200,
                data: userWithNewAddress,
                message: ' Adress Added '
            }
            
        } catch (error) {
            return {
                status: 400,
                data: error,
                message: ' Adress not Added '
            }
        }
    }
}

module.exports = usersService