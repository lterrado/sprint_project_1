const path = require('path')
const passport = require('passport')
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
const linkedinConfig = require(path.join(__dirname,'../../config/index')).config


passport.use('linkedin',new LinkedInStrategy({
    clientID: linkedinConfig.linkedinKey,
    clientSecret: linkedinConfig.linkedinSecret,
    callbackURL: linkedinConfig.linkedinCallback,
    scope: ['r_emailaddress', 'r_liteprofile'],
},
function(request, accessToken, refreshToken, profile, done) {
  return done(null, profile);
}));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
})
