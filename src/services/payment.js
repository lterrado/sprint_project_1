const paymentsModel = require('../library/model/payments')

class PaymentsServices{

    async getPayments() {
        const payments = await paymentsModel.find({}).lean()

        return payments || []
    }

    async newPayment(payment_name) {

        try {
            const newPayment = await paymentsModel.create({payment_name})
            return {
                status: 200,
                data: newPayment,
                message: 'New Payment added'
            }
        } catch (err) {
            return {
                status: 400,
                data: err,
                message: 'New Payment couldn´t be added'
            }
        }

    }

    async deletePayment(id) {
        
        try {
            const deletePayment = await paymentsModel.findByIdAndDelete(id, {new: true})
            return {
                status: 200,
                data: deletePayment,
                message: 'Payment deleted'
            }
        } catch (err) {
            return {
                status: 400,
                data: err,
                message: 'Payment couldn`t be deleted'
            }
        }
    }

}

module.exports = PaymentsServices