const jwt = require('jsonwebtoken')
const { config } = require('../../config/index')
const ProductsModel = require('../library/model/products')
const UserModel = require('../library/model/users')


const verifyAuth = (req, res, next) => {
    
    const token = req.headers.authorization.split(' ')[1]
    
    if (!token) res.status(401).json({ error: 'Access Denied' })
    
    try {
        const verifiedUser = jwt.verify(token, config.jwtToken)
        req.user = verifiedUser
        next() 
    } catch (error) {
        res.status(400).json({error: 'Token not valid'})
    }
    
}


const isAdmin = async (req,res,next) => {
    let  userId = req.user.id

    const adminUser = await UserModel.findById(userId).exec()
    if (adminUser.admin){
        console.log('Admin validated, Permission Granted')
        next()
    } 
    else {
        res.status(400).json({
            data: null , 
            message:`user does not have Admin permission` 
        })
    }
}

const isEmpty = async (req,res,next) => {
    
    if (req.body) {
        let bodyArray = Object.values(req.body)

        let verifiedEmptyArray = ( bodyItem ) => bodyItem == null || bodyItem == undefined || bodyItem == ""
        const bodyArrayFull = bodyArray.filter(verifiedEmptyArray)

        if (bodyArrayFull.length == 0) {
            console.log('Data complete')
            next()
        }
        else {
            res.status(400).send('Data is missing')
        }
    }
}



module.exports = {
    isAdmin,
    verifyAuth,
    isEmpty
};