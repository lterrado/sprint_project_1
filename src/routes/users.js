const router = require('express').Router()
const mW = require('../middlewares/middlewares')

const UserServices = require('../services/users')
const userServices = new UserServices()


router.get('/', async (req, res,next) => {
    try {
        const users = await userServices.getUsers()

        res.status(200).json({
            data: users,
            message:'users listed'
        })
    } catch (error) {
        next(error)
    }
})

router.post('/reg', async (req,res) => {
    
    let {user, name, lastName,email,phoneNumber,address,pass} = req.body

    let newUserData = await userServices.newUsers(user, name, lastName,email,phoneNumber,address, pass)
    
    res.status(newUserData.status).json({
        data: newUserData.data,
        message: newUserData.message
    })
})


router.post('/login', async (req, res) => {
    let {user, pass} = req.body

    let logUsers = await userServices.login(user,pass)
    
    res.status(logUsers.status).json({
        data: logUsers.data,
        message: logUsers.message
    })
    
})

router.post('/address', mW.verifyAuth, async (req, res) => {
    let {address} = req.body
    let userId = req.user.id

    let newAddress = await userServices.setNewAddress(userId, address)

    res.status(newAddress.status).json({
        data: newAddress.data,
        message: newAddress.message
    })
})

module.exports = router