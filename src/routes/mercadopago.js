const router = require('express').Router()
const mercadopago = require('mercadopago');
const config = require('../../config').config


router.get('/payment/token', (req, res) => {
    res.json({
        token: config.mercadoPagoPublic
    })
})

router.post('/process_payment', (req, res) => {
    mercadopago.configurations.setAccessToken(config.mercadoPagoAccessToken);
    mercadopago.payment.save(req.body)
        .then(function (response) {
        const { status, status_detail, id } = response.body;
        res.status(response.status).json({ status, status_detail, id });
        })
        .catch(function (error) {
        console.error(error);
        res.status(406).json({status: 406, status_detail: 'Error on payment'})
        });
});


module.exports = router