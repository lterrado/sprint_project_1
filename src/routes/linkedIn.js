const path = require('path')
const router  = require("express").Router()
const passport = require('passport')
require(path.join(__dirname, '../services/linkedinStrategy'))

router.get('/auth', passport.authenticate('linkedin', { state: 'SOME STATE'  }))

router.get('/callback',
        passport.authenticate('linkedin', {
                successRedirect: 'login',
                failureRedirect: '/'
        })
)

router.get('/login', (req, res) => {
  res.send(`Hello and Welcome to Delilah`);
});


module.exports = router
