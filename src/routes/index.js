const router = require('express').Router()

const usersApi = require('./users')
const productsApi = require('./products')
const ordersApi = require('./orders')
const paymentsApi = require('./paymets')
const linkedinOauth = require('./linkedIn')
const googleOauth = require('./google')
const facebookOauth = require('./facebook')


router.use('/users', usersApi)
router.use('/products', productsApi)
router.use('/orders', ordersApi)
router.use('/payment', paymentsApi)
router.use('/linkedin', linkedinOauth)
router.use('/google', googleOauth)
router.use('/facebook', facebookOauth)

module.exports = router