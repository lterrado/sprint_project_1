const path = require('path')
const router = require("express").Router()
const passport = require('passport')
require(path.join(__dirname,'../services/facebookStrategy'))

router.get('/auth', passport.authenticate('facebook'));

router.get('/callback',
        passport.authenticate('facebook', {
                successRedirect: 'login',
                failureRedirect: '/'
        })
)

router.get('/login', (req, res) => {
  res.send(`Hello and Welcome to Delilah`);
});


module.exports = router
