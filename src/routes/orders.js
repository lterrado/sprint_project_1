const router = require('express').Router()
const mW = require('../middlewares/middlewares')
const userData = require('../../utils/userData')


const OrdersServices = require('../services/orders')
const orderServices = new OrdersServices()
const userInfo = require('../../utils/userData')


router.get('/listarXUser',mW.verifyAuth,  async (req,res) => {
    
    try {
        const userInfo = await userData(req.user.id)

        const ordersListed = await orderServices.getOrders(userInfo)
    
        res.status(200).json({
            data: ordersListed,
            message: `${req.user.username} orders listed`
        })
    } catch (error) {
        console.error(error)
    }

})


router.post('/new_order', mW.verifyAuth, async (req,res) => {

    const userId = req.user.id

    let {order_details, paymentsId, addressDelivery} = req.body

    const newOrder = await orderServices.createOrder(userId, order_details, order_State = "nuevo" , paymentsId, addressDelivery)
    
    res.status(newOrder.status).json({
        data: newOrder.data,
        message: newOrder.message
    })
})


router.patch('/modifica_estado', mW.verifyAuth, mW.isAdmin, async(req, res) => {
    let {orderId, orderState} = req.body

    const newState = await orderServices.changeOrderState(orderId, orderState)

    res.status(newState.status).json({
        data: newState.data,
        message: newState.message
    })
})

module.exports = router