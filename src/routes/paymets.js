const router = require('express').Router()
const mW = require('../middlewares/middlewares')

const PaymentsServices = require('../services/payment')
const paymentServices = new PaymentsServices()


router.post('/nuevo', mW.verifyAuth, mW.isAdmin,  async (req, res) => {
    
    let {payment_name} = req.body

    let newPayment = await paymentServices.newPayment(payment_name)

    res.status(newPayment.status).json({
        data:newPayment.data,
        message: newPayment.message
    })

})



router.get('/listar',mW.verifyAuth, mW.isAdmin, async (req, res) => {
    
    const paymentsList = await paymentServices.getPayments()

    res.status(200).json({
        data: paymentsList,
        message:'Payments listed'
    })
})



router.delete('/borrar', mW.verifyAuth, mW.isAdmin , async (req,res) => {
    console.log(req.body)
    let {id} = req.body

    const paymentDeleted = await paymentServices.deletePayment(id)

    res.status(paymentDeleted.status).json({
        data: paymentDeleted.data,
        message: paymentDeleted.message
    })
    
})


module.exports = router