const path = require('path')
const router = require('express').Router();
const passport = require('passport')
require(path.join(__dirname,'../services/googleStrtegy'))


router.get('/auth',
    passport.authenticate('google', { scope: ['profile'] })
);

router.get('/callback',
        passport.authenticate('google', {
                successRedirect: 'login',
                failureRedirect: '/'
        })
);

router.get('/login', (req, res) => {
        res.send(`Welcome to Delilah`);
});

module.exports = router
