const router = require('express').Router()
const mW = require('../middlewares/middlewares')

const ProductsService = require('../services/products')
const productService = new ProductsService()


router.get('/', mW.verifyAuth, async (req,res,next) => {
    
    try {
        const products = await productService.getProducts()
        res.status(200).json({
            data: products,
            message:'products listed'
        })
    } catch (error) {
        res.status(400).json({
            data: error,
            message: 'products couldn`t be listed'
        })
    }

})

router.post('/altaProducto', mW.isEmpty, mW.verifyAuth, mW.isAdmin, async (req, res) => {
    
    let {food_name, food_price} = req.body

    const newProduct = await productService.newProduct(food_name, food_price)

    res.status(newProduct.status).json({
        data:newProduct.data,
        message: newProduct.message
    })

})


router.patch('/modificaProducto/', mW.isEmpty, mW.verifyAuth, mW.isAdmin, async (req, res) => {
    
    let {id, food_name, food_price} = req.body


     const productModified = await productService.modifiedProduct(id, food_name, food_price)

    res.status(productModified.status).json({

        data: productModified.data,
        message: productModified.message

    })
})



router.delete('/borraProducto', mW.isEmpty, mW.verifyAuth, mW.isAdmin, async (req,res) => {
    let {id} = req.body

    let productDeleted = await productService.deleteProduct(id)
    
    res.status(productDeleted.status).json({
    
        data: productDeleted.data,
        message: productDeleted.message
        
    })
})


module.exports = router