const mongoose = require('mongoose');
const { config } = require('../../config/index')


const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}


const USER = encodeURIComponent(config.dbUser)
const PASSWORD = encodeURIComponent(config.dbPassword)
const DB_NAME = config.dbName
const DB_HOST = config.dbHost
const dockerURI = process.env.MONGODB_CONNSTRING

const MONGO_URI =  dockerURI || `mongodb+srv://${USER}:${PASSWORD}@${DB_HOST}/${DB_NAME}?retryWrites=true&w=majority`


mongoose.connect(MONGO_URI, options )
    .then(()=> console.log(`MongoDB default connection open to ${DB_HOST}`)) 
    .catch(err => console.log(`MongoDB default connection error: ${err}`))


// When the connection is disconnected
mongoose.connection.on('disconnected', () => { 
    console.log('MongoDB default connection disconnected'); 
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', () => {   
    mongoose.connection.close( () => { 
        console.log('Mongoose default connection disconnected through app termination'); 
        process.exit(0); 
    }); 
}); 


module.exports = mongoose;
