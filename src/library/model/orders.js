var mongoose = require('../connection');
var Schema = mongoose.Schema;

const reqString = {
    type: String, 
    required: true 
}

const OrderDetailsSchema = new Schema({ 
    product_id: reqString,
    quantity: {
        type: Number,
        required: true
    }
});

const statesOfOrders = [
    "nuevo",
    "confirmado",
    "preparando",
    "enviando",
    "entregando"
]


const OrdersSchema = new Schema({
    userId: reqString,
    order_details: [OrderDetailsSchema],
    order_State: {
        type: String,
        enum: statesOfOrders,
        required: true
    },
    paymentsId: reqString,
    addressDelivery: reqString,
},{
    timestamps: true,
}
)

module.exports = mongoose.model('orders', OrdersSchema)