var mongoose = require('../connection');
var Schema = mongoose.Schema;

const reqString = {
    type: String, 
    required: true 
}


let UserSchema = new Schema({
    user: reqString,
    pass:reqString,
    name: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    lastName: reqString,
    email: {
        type: String,
        required: true,
        min: 6,
        max: 1024
    },
    phoneNumber: reqString,
    address: [],
    admin: {
        type: Boolean,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    }
},{
    timestamps: true,
}
)

module.exports = mongoose.model('User', UserSchema)