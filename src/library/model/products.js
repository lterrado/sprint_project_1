var mongoose = require('../connection');
var Schema = mongoose.Schema;

let ProductsSchema = new Schema({
    
    food_name: { 
        type: String, 
        required: true 
    },
    food_price: { 
        type: Number, 
        required: true 
    }    
},{
    timestamps:true
}
)

module.exports = mongoose.model('Products', ProductsSchema)