var mongoose = require('../connection');
var Schema = mongoose.Schema;

let PaymentsSchema = new Schema({

    payment_name : {
        type: String, 
        required: true 
    },
    date_created: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('payments', PaymentsSchema)