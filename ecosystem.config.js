module.exports = {
    apps : [
        {
          name: "Delilah_Restó",
          script: "./server.js",
          watch: true,
          env: {
              "PORT": 3001,
              "NODE_ENV": "dev",
              "DOMAIN":"localhost"
          },
          env_production: {
              "PORT": 3000,
              "NODE_ENV": "production",
              "DOMAIN": "https://wwww.leanterrado.tk"
          }
        }
    ]
}