const chai = require('chai');
const fetch = require('node-fetch');


const url= 'http://localhost:3001/api/users/reg';

const body = {
    "user": "RocioTerrado",
    "name": "Rocio",
    "lastName": "Terrado",
    "email": "roterrado@gmail.com",
    "phoneNumber": "111222111",
    "address": "Por acá 200",
    "pass": "mono"
}

const bodyFail = {}

describe('Pruebas de consumo de API Usuarios', () => {
  
    it('User Registration sucessfull', async () => {
      const {status} = await fetch(url, {
        method: 'post',
        body: JSON.stringify(body),
        headers: {'Content-Type': 'application/json'}
      });
      
      chai.expect(status).to.be.equal(200);
      
    })

})