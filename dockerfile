FROM node:16.2.0-alpine

COPY . .
RUN npm install .
EXPOSE 3001
CMD node server.js