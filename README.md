# Delilah Restó

REST API for the Restaurant Delilah Restó mounted on AWS services.

## Lessons Learned

On this project was awsome to:

- Work with Mongo and Mongo Atlas was nice. Learning to use Colections and Documents instead of Tables and records.
- Use Mongoose as ODM to work with Mongo databases.
- How to use Docker to share my App.
- Create the infrastructure on AWS services: 
    - Creating EC2 instances
    - Auto-scaling, 
    - Balancers
    - HTTPS certificates
    - Creating free Domains 
    - Deploying my app on an Ubuntu terminal connected via SSH.
- Used Passport.js to connect with IdP such as: Google, Facebook, LinkedIn.
- Connect to MercadoPago as a payment gateway.
- Create a API with Node.js and Express.js.
- Mocha and Chai for Unit Test
- JWT and bcryptjs for Auth and Password encryptation

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/lterrado/sprint_project_1
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run dev
  or
  pm2 start ecosystem.config.js --env dev
```

## Docker
Mounting image example;
```bash
docker build -t delilah:4.0 .
```
runnin container
```bash
docker run -d --name <container_name> -e MONGODB_CONNSTRING=mongodb+srv://<user>:<pass>@<server>/<database> -p 3001:3001 <image_name>
```

## Deployment

To run this project

```bash
  npm run start
  or
  pm2 start ecosystem.config.js --env production
```


## Appendix

- IdP will work only on https://www.leanterrado.tk.
- Documentations is located on: <domain>/api-docs
    - Choose http / https according local or public domain
    - When use JWT token, remember to add Bearer at the beginning
- For MercadoPago Card info use:
    
    - name: Martinez Mirtha
    - email: test_user_99968301@testuser.com
    - documento: 23011111114
    - Test Card:
        - Number: 5031 7557 3453 0604
        - CVV: 123
        - Date: 11/25

## Authors

- [@lterrado](https://gitlab.com/lterrado)

## Badges

[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)
