const swaggerJSDoc = require('swagger-jsdoc');
const port = require('../config/index').config.port


//Opciones Swagger
const swaggerOptions = {
    swaggerDefinition:{
        openapi: "3.0.0",
        info: {
            title: "Sprint Project 2 API",
            description: "API para el Delilah Restó",
            version: "2.0.0",
            contact: {
                name: "Leandro Terrado",
                email: "lterrado@gmail.com",
                url: "",
            },
        },
        servers: [
            {
                url:`http://localhost:${port}`
            }
        ]
    },
    components: {
        securitySchemes: {
          jwt: {
            type: "http",
            scheme: "bearer",
            in: "header",
            bearerFormat: "JWT"
          },
        }
      }
      ,
      security: [{
        jwt: []
      }],
    swagger: "2.0",
    apis:['../src/routes/index.js'],
};
const swaggerDocs = swaggerJSDoc(swaggerOptions);







module.exports = swaggerDocs