const bcrypt = require('bcrypt')


function encrypt (plainPass) {
    return bcrypt.hash(plainPass, 10)
}

function verifiedEncryptedPass (plainPass, hashPass) {
    return bcrypt.compare(plainPass,hashPass)
}

module.exports = {
    encrypt,
    verifiedEncryptedPass
}