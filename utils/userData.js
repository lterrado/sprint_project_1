const usersModel = require('../src/library/model/users')

async function userDetails(userId) {
    try {
        const userInfo = await usersModel.findById(userId)
        return userInfo
    } catch (error) {
        return error
    }

}

module.exports = userDetails