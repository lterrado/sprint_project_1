require('dotenv').config()
const domain = process.env.DOMAIN || 'http://localhost'

const config = {
    dev: process.env.NODE_ENV !== 'production',
    port: process.env.PORT || 3001,
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASSWORD,
    dbHost: process.env.DB_HOST,
    dbName: process.env.DB_NAME,
    jwtToken: process.env.TOKEN_SECRET,
    linkedinKey: process.env.LINKEDIN_KEY,
    linkedinSecret: process.env.LINKEDIN_SECRET,
    linkedinCallback: `${domain}${process.env.LINKEDIN_CALLBACK}`,
    googleCallback: `${domain}${process.env.GOOGLE_CALLBACK}`,
    googleClientID: process.env.GOOGLE_CLIENT_ID,
    googleSecretID: process.env.GOOGLE_CLIENT_SECRET,
    facebookCallback: `${domain}${process.env.FACEBOOK_CALLBACK}`,
    facebookClientID: process.env.FACEBOOK_CLIENT_ID,
    facebookClientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    mercadoPagoPublic: process.env.MERCADOPAGO_PUBLIC_TOKEN,
    mercadoPagoAccessToken: process.env.MERCADOPAGO_ACCESS_TOKEN
}

module.exports = { config,domain }